package com.ahu.xrq.demo.dao;

import static org.junit.Assert.assertEquals;

import java.util.Date;
import java.util.List;

import com.ahu.xrq.demo.entity.Area;
import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING) // 按方法名大小升序执行
public class AreaDaoTest {
	//通过spring容器注入Dao的实现类
	@Autowired
	private AreaDao areaDao;

	@Test
	@Ignore
	public void testAQueryArea() {
		List<Area> areaList = areaDao.queryArea();
		// 验证预期值和实际值是否相符
		assertEquals(2, areaList.size());
	}

	@Test
	@Ignore
	public void testBInsertArea() {
		Area area = new Area();
		area.setAreaName("榴园");
		area.setPriority(1);
		int effectNum = areaDao.insertArea(area);
		assertEquals(1,effectNum);
	}

	@Test
	@Ignore
	public void testCQueryAreaById() {
		Area area = areaDao.queryAreaById(2);
		assertEquals("桂园", area.getAreaName());
	}

	@Test
	@Ignore
	public void testDUpateArea() {
		Area area = new Area();
		area.setAreaName("榴园");
		area.setAreaId(3);
		area.setLastEditTime(new Date());
		int effectNum = areaDao.updateArea(area);
		assertEquals(1,effectNum);
	}

	@Test
	public void testEDeleteArea() {
		int effectNum = areaDao.deleteArea(3);
		assertEquals(1,effectNum);
	}
}