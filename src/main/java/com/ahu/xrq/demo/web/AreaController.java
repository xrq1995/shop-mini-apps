package com.ahu.xrq.demo.web;

import com.ahu.xrq.demo.entity.Area;
import com.ahu.xrq.demo.service.AreaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/superadmin")
public class AreaController {
	private final AreaService areaService;

	@Autowired
	public AreaController(AreaService areaService) {
		this.areaService = areaService;
	}

	//返回所有数据
	@RequestMapping(value = "/listarea", method = RequestMethod.GET)
	private Map<String, Object> listArea() {
		Map<String, Object> modelMap = new HashMap<String, Object>();
		List<Area> list = areaService.getAreaList();
		modelMap.put("areaList", list);
		return modelMap;
	}

	//通过id获取数据
	@RequestMapping(value = "/getareabyid", method = RequestMethod.GET)
	private Map<String, Object> getAreaById(Integer areaId) {
		//定义map来存放对象
		Map<String, Object> modelMap = new HashMap<>();
		Area area = areaService.getAreaById(areaId);
		modelMap.put("area", area);
		return modelMap;
	}

	//添加数据
	@RequestMapping(value = "addarea", method = RequestMethod.POST)
	private Map<String, Object> addArea(@RequestBody Area area) {
		Map<String, Object> modelMap = new HashMap<>();
		modelMap.put("success", areaService.addArea(area));
		return modelMap;
	}

	//添加数据  待修改
	@RequestMapping(value = "modifyarea", method = RequestMethod.PUT)
	private Map<String, Object> modifyArea(@RequestBody Area area) {
		Map<String, Object> modelMap = new HashMap<>();
		modelMap.put("success", areaService.modifyArea(area));
		return modelMap;
	}

	//删除数据
	@RequestMapping(value = "removearea", method = RequestMethod.GET)
	private Map<String, Object> removeArea(Integer areaId) {
		Map<String, Object> modelMap = new HashMap<>();
		modelMap.put("success", areaService.deleteArea(areaId));
		return modelMap;
	}
	//所有的异常处理交由统一异常类来处理
}
